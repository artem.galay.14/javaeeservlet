package com.inno.servlet;

import com.inno.dao.ClientDao;
import com.inno.pojo.Client;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/addclient")
public class AddClientServlet extends HttpServlet {

    private ClientDao clientDao;

    @Override
    public void init() throws ServletException {
        clientDao = (ClientDao) getServletContext().getAttribute("clientDao");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("PageTitle", "New Client");
        req.setAttribute("PageBody", "form-client.jsp");
        req.getRequestDispatcher("/layout.jsp")
            .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.setCharacterEncoding("utf-8");
        String name = req.getParameter("name");
        String phoneNumber = req.getParameter("phoneNumber");
        String email = req.getParameter("email");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String role = req.getParameter("role");
        Client client = new Client(null, name, phoneNumber, email, login, password, Client.ROLE.valueOf(role));
        clientDao.addClient(client);

        HttpSession session = req.getSession();
        session.setAttribute("login", login);

        resp.sendRedirect(req.getContextPath() + "/");
    }
}
