package com.inno.servlet;

import com.inno.dao.ClientDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/deleteclient")
public class DeleteClientServlet extends HttpServlet {

    private ClientDao clientDao;

    @Override
    public void init() throws ServletException {
        clientDao = (ClientDao) getServletContext().getAttribute("clientDao");
        super.init();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.setCharacterEncoding("utf-8");
        String clientId = req.getParameter("id");
        clientDao.deleteClientById(Integer.valueOf(clientId));

        resp.sendRedirect(req.getContextPath() + "/allclients");
    }
}
