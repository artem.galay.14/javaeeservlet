package com.inno.servlet;

import com.inno.dao.ArtDao;
import com.inno.pojo.Art;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/addart")
public class AddArtServlet extends HttpServlet {

    private ArtDao artDao;

    @Override
    public void init() throws ServletException {
        artDao = (ArtDao) getServletContext().getAttribute("dao");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("PageTitle", "New Art");
        req.setAttribute("PageBody", "form.jsp");
        req.getRequestDispatcher("/layout.jsp")
            .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.setCharacterEncoding("utf-8");
        String name = req.getParameter("name");
        String price = req.getParameter("price");
        Art art = new Art(null, name, Double.valueOf(price));
        artDao.addArt(art);

        resp.sendRedirect(req.getContextPath() + "/allarts");
    }
}
