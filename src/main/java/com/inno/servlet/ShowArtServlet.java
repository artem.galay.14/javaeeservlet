package com.inno.servlet;

import com.inno.dao.ArtDao;
import com.inno.pojo.Art;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/showart")
public class ShowArtServlet extends HttpServlet {

    private ArtDao artDao;

    @Override
    public void init() throws ServletException {
        artDao = (ArtDao) getServletContext().getAttribute("dao");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String artId = req.getParameter("id");
        if (artId == null) {
            throw new ServletException("Missing parameter id");
        }
        Art art = artDao.getArtById(Integer.valueOf(artId));
        if (art == null) {
            resp.setStatus(404);
            req.setAttribute("PageTitle", "Arts");
            req.setAttribute("PageBody", "notfound.jsp");
            req.getRequestDispatcher("/layout.jsp")
                    .forward(req, resp);
            return;
        }
        req.setAttribute("art", art);
        req.setAttribute("PageTitle", "Arts");
        req.setAttribute("PageBody", "showart.jsp");
        req.getRequestDispatcher("/layout.jsp")
                .forward(req, resp);
    }
}
