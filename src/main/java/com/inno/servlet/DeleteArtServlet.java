package com.inno.servlet;

import com.inno.dao.ArtDao;
import com.inno.pojo.Art;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/deleteart")
public class DeleteArtServlet extends HttpServlet {

    private ArtDao artDao;

    @Override
    public void init() throws ServletException {
        artDao = (ArtDao) getServletContext().getAttribute("dao");
        super.init();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.setCharacterEncoding("utf-8");
        String artId = req.getParameter("id");
        artDao.deleteArtById(Integer.valueOf(artId));

        resp.sendRedirect(req.getContextPath() + "/allarts");
    }
}
