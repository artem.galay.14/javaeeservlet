package com.inno.servlet;

import com.inno.dao.ClientDao;
import com.inno.pojo.Client;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/updateclient")
public class UpdateClientServlet extends HttpServlet {

    private ClientDao clientDao;

    @Override
    public void init() throws ServletException {
        clientDao = (ClientDao) getServletContext().getAttribute("clientDao");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Client client = clientDao.getClientById(Integer.valueOf(req.getParameter("id")));

        req.setAttribute("client", client);
        req.setAttribute("PageTitle", "Update Client");
        req.setAttribute("PageBody", "formupdate-client.jsp");
        req.getRequestDispatcher("/layout.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.setCharacterEncoding("utf-8");
        String clientId = req.getParameter("id");
        String name = req.getParameter("name");
        String phoneNumber = req.getParameter("phoneNumber");
        String email = req.getParameter("email");
        Client updClient = new Client(null, name, phoneNumber, email, null, null, null);
        clientDao.updateClientById(Integer.valueOf(clientId), updClient);

        resp.sendRedirect(req.getContextPath() + "/allclients");
    }
}