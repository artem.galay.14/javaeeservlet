package com.inno.servlet;

import com.inno.dao.ArtDao;
import com.inno.pojo.Art;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@WebServlet(urlPatterns = "/allarts", name = "Arts")
public class AllArtsServlet extends HttpServlet {
    private ArtDao artDao;
    private Logger logger = LoggerFactory.getLogger(AllArtsServlet.class);

    @Override
    public void init() throws ServletException {
        artDao = (ArtDao) getServletContext().getAttribute("dao");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Collection<Art> arts = artDao.getAllArt();
        req.setAttribute("arts", arts);
        req.setAttribute("PageTitle", "Arts");
        req.setAttribute("PageBody", "allarts.jsp");
        req.getRequestDispatcher("/layout.jsp")
            .forward(req, resp);
    }
}
