package com.inno.servlet;

import com.inno.dao.ClientDao;
import com.inno.pojo.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@WebServlet(urlPatterns = "/allclients", name = "Clients")
public class AllClientsServlet extends HttpServlet {
    private ClientDao clientDao;
    private Logger logger = LoggerFactory.getLogger(AllClientsServlet.class);

    @Override
    public void init() throws ServletException {
        clientDao = (ClientDao) getServletContext().getAttribute("clientDao");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Collection<Client> clients = clientDao.getAllClient();
        req.setAttribute("clients", clients);
        req.setAttribute("PageTitle", "Clients");
        req.setAttribute("PageBody", "allclients.jsp");
        req.getRequestDispatcher("/layout.jsp")
            .forward(req, resp);
    }
}
