package com.inno.servlet;

import com.inno.dao.ClientDao;
import com.inno.pojo.Client;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/showclient")
public class ShowClientServlet extends HttpServlet {

    private ClientDao clientDao;

    @Override
    public void init() throws ServletException {
        clientDao = (ClientDao) getServletContext().getAttribute("clientDao");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String clientId = req.getParameter("id");
        if (clientId == null) {
            throw new ServletException("Missing parameter id");
        }
        Client client = clientDao.getClientById(Integer.valueOf(clientId));
        if (client == null) {
            resp.setStatus(404);
            req.setAttribute("PageTitle", "Clients");
            req.setAttribute("PageBody", "notfound.jsp");
            req.getRequestDispatcher("/layout.jsp")
                    .forward(req, resp);
            return;
        }
        req.setAttribute("client", client);
        req.setAttribute("PageTitle", "Clients");
        req.setAttribute("PageBody", "showclient.jsp");
        req.getRequestDispatcher("/layout.jsp")
                .forward(req, resp);
    }
}
