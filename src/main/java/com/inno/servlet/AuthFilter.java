package com.inno.servlet;

import com.inno.dao.ClientDao;
import com.inno.dao.ClientDaoImpl;
import com.inno.pojo.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class AuthFilter implements Filter {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain filterChain)

            throws IOException, ServletException {

        LOGGER.debug("Enter AuthFilter");

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        HttpSession session = req.getSession();

        String login = (String) session.getAttribute("login");
        //String password = (String) session.getAttribute("password");

        if (login != null) {
            filterChain.doFilter(request, response);
        } else {
            session.setAttribute("login", "guest");
            req.setAttribute("PageTitle", "Authorization");
            req.setAttribute("PageBody", "auth.jsp");
            req.getRequestDispatcher("/layout.jsp")
                    .forward(req, res);
        }
    }

    @Override
    public void destroy() {
    }

}
