package com.inno.servlet;

import com.inno.dao.ClientDao;
import com.inno.pojo.Client;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/checkclient")
public class CheckClientServlet extends HttpServlet {

    private ClientDao clientDao;

    @Override
    public void init() throws ServletException {
        clientDao = (ClientDao) getServletContext().getAttribute("clientDao");
        super.init();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        HttpSession session = req.getSession();

        if (clientDao.clientIsExist(login, password)) {
            session.setAttribute("login", login);

            req.setAttribute("PageTitle", "Main Page");
            req.setAttribute("PageBody", "index.jsp");
            req.getRequestDispatcher("/layout.jsp")
                    .forward(req, resp);
        } else {
            session.setAttribute("login", null);
            req.setAttribute("PageTitle", "Error Page");
            req.setAttribute("PageBody", "error.jsp");
            req.getRequestDispatcher("/layout.jsp")
                    .forward(req, resp);
        }
    }
}