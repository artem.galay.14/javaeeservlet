package com.inno.dao;

import com.inno.ConnectionManager.ConnectionManager;
import com.inno.pojo.Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@EJB
public class ClientDaoImpl implements ClientDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(ClientDaoImpl.class);
    public static final String INSERT_INTO_CLIENT = "INSERT INTO t_clients values (DEFAULT, ?, ?, ?, ?, ?, ?)";
    public static final String SELECT_FROM_CLIENT = "SELECT * FROM t_clients WHERE client_id = ?";
    public static final String UPDATE_CLIENT = "UPDATE t_clients SET name=?, phone_number=?, email=? WHERE client_id=?";
    public static final String DELETE_FROM_CLIENT = "DELETE FROM t_clients WHERE client_id=?";
    public static final String SELECT_ALL_FROM_CLIENT= "SELECT client_id, name, phone_number, email FROM t_clients";
    public static final String SELECT_ROLE_BY_LOGIN_FROM_CLIENT = "SELECT role FROM t_clients WHERE login=? and password=?";

    private ConnectionManager connectionManager;

    @Inject
    public ClientDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public boolean addClient(Client client) {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO_CLIENT)) {
            preparedStatement.setString(1, client.getName());
            preparedStatement.setString(2, client.getPhoneNumber());
            preparedStatement.setString(3, client.getEmail());
            preparedStatement.setString(4, client.getLogin());
            preparedStatement.setString(5, client.getPassword());
            preparedStatement.setString(6, client.getRole().toString());
            preparedStatement.execute();
        } catch (SQLException e) {
            LOGGER.error("Error in addClient method", e);
            return false;
        }
        return true;
    }

    @Override
    public Client getClientById(Integer id) {
        LOGGER.debug("Enter getClientById");
        LOGGER.debug("id " + id);
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FROM_CLIENT)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                LOGGER.debug("executeQuery");
                if (resultSet.next()) {
                    Client newClient = new Client(
                            resultSet.getInt(1),
                            resultSet.getString(2),
                            resultSet.getString(3),
                            resultSet.getString(4),
                            resultSet.getString(5),
                            resultSet.getString(6),
                            Client.ROLE.valueOf(resultSet.getString(7))
                    );
                    LOGGER.debug("newClient: " + newClient);
                    return newClient;
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Error in getClientById method", e);
        }
        return null;
    }

    @Override
    public void deleteClientById(Integer id) {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_FROM_CLIENT)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            LOGGER.error("Error in deleteClientById method", e);
        }
    }

    @Override
    public void updateClientById(Integer id, Client updClient) {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_CLIENT)) {
            preparedStatement.setString(1, updClient.getName());
            preparedStatement.setString(2, updClient.getPhoneNumber());
            preparedStatement.setString(3, updClient.getEmail());
            preparedStatement.setInt(4, id);
            preparedStatement.executeQuery();
        } catch (SQLException e) {
            LOGGER.error("Error in updateClientById method", e);
        }
    }

    @Override public List<Client> getAllClient() {
        List<Client> lst = new ArrayList<>();
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_FROM_CLIENT);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                lst.add(new Client(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        null,
                        null,
                        null));
            }
            return lst;
        } catch (SQLException e) {
            LOGGER.error("Error in getAllClient method", e);
        }
        return new ArrayList<>();
    }

    @Override
    public Client.ROLE getRoleByLoginPassword(String login, String password) {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ROLE_BY_LOGIN_FROM_CLIENT)) {
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return Client.ROLE.valueOf(resultSet.getString(1));
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Error in getRoleByLoginPassword method", e);
        }
        return Client.ROLE.UNKNOWN;
    }

    @Override
    public boolean clientIsExist(String login, String password) {
        LOGGER.debug("Enter clientIsExist");
        if (login == null || password == null)
            return false;

        try (Connection connection = connectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ROLE_BY_LOGIN_FROM_CLIENT)) {
            LOGGER.debug("Enter SELECT_ROLE_BY_LOGIN_FROM_CLIENT");
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, password);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return true;
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Error in getRoleByLoginPassword method", e);
        }
        return false;
    }
}
