package com.inno.dao;

import com.inno.pojo.Client;

import java.util.List;

public interface ClientDao {
    boolean addClient(Client client);

    Client getClientById(Integer id);

    Client.ROLE getRoleByLoginPassword(String login, String password);

    boolean clientIsExist(String login, String password);

    void deleteClientById(Integer id);

    void updateClientById(Integer id, Client updClient);

    List<Client> getAllClient();
}
