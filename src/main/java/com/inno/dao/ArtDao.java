package com.inno.dao;


import com.inno.pojo.Art;

import java.util.List;

public interface ArtDao {
  boolean addArt(Art art);

  Art getArtById(Integer id);

  void deleteArtById(Integer id);

  void updateArtById(Integer id, Art updArt);

  List<Art> getAllArt();
}
