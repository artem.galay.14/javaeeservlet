package com.inno.dao;

import com.inno.ConnectionManager.ConnectionManager;
import com.inno.pojo.Art;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@EJB
public class ArtDaoImpl implements ArtDao {
  private static final Logger LOGGER = LoggerFactory.getLogger(ArtDaoImpl.class);
  public static final String INSERT_INTO_ART = "INSERT INTO t_art values (DEFAULT, ?, ?)";
  public static final String SELECT_FROM_ART = "SELECT * FROM t_art WHERE art_id = ?";
  public static final String UPDATE_ART = "UPDATE t_art SET name=?, price=? WHERE art_id=?";
  public static final String DELETE_FROM_ART = "DELETE FROM t_art WHERE art_id=?";
  public static final String SELECT_ALL_FROM_ART= "SELECT * FROM t_art";

  private ConnectionManager connectionManager;

  @Inject
  public ArtDaoImpl(ConnectionManager connectionManager) {
    this.connectionManager = connectionManager;
  }

  @Override
  public boolean addArt(Art art) {
    try (Connection connection = connectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_INTO_ART)) {
      preparedStatement.setString(1, art.getName());
      preparedStatement.setDouble(2, art.getPrice());
      preparedStatement.execute();
    } catch (SQLException e) {
      LOGGER.error("Error in addArt method", e);
      return false;
    }
    return true;
  }

  @Override
  public Art getArtById(Integer id) {
    try (Connection connection = connectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FROM_ART)) {
      preparedStatement.setInt(1, id);
      try (ResultSet resultSet = preparedStatement.executeQuery()) {
        if (resultSet.next()) {
          return new Art(
              resultSet.getInt(1),
              resultSet.getString(2),
              resultSet.getDouble(3));
        }
      }
    } catch (SQLException e) {
      LOGGER.error("Error in getArtById method", e);
    }
    return null;
  }

  @Override
  public void deleteArtById(Integer id) {
    try (Connection connection = connectionManager.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(DELETE_FROM_ART)) {
      preparedStatement.setInt(1, id);
      preparedStatement.executeQuery();
    } catch (SQLException e) {
      LOGGER.error("Error in deleteArtById method", e);
    }
  }

  @Override
  public void updateArtById(Integer id, Art updArt) {
    try (Connection connection = connectionManager.getConnection();
         PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_ART)) {
      preparedStatement.setString(1, updArt.getName());
      preparedStatement.setDouble(2, updArt.getPrice());
      preparedStatement.setInt(3, id);
      preparedStatement.executeQuery();
    } catch (SQLException e) {
      LOGGER.error("Error in updateArtById method", e);
    }
  }

  @Override public List<Art> getAllArt() {
    List<Art> lst = new ArrayList<>();
    try (Connection connection = connectionManager.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_FROM_ART);
        ResultSet resultSet = preparedStatement.executeQuery()) {
      while (resultSet.next()) {
        lst.add(new Art(
            resultSet.getInt(1),
            resultSet.getString(2),
            resultSet.getDouble(3)));
      }
      return lst;
    } catch (SQLException e) {
      LOGGER.error("Error in getAllArt method", e);
    }
    return new ArrayList<>();
  }
}
