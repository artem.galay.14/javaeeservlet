<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean id="client" class="com.inno.pojo.Client" />
    <c:set target="${client}" property="name" value="NewClient" />
    <jsp:setProperty name="client" property="phoneNumber" value="12345" />
    <jsp:setProperty name="client" property="email" value="client@gmail.com" />
    <jsp:setProperty name="client" property="login" value="login" />
    <jsp:setProperty name="client" property="password" value="qwerty" />
    <jsp:setProperty name="client" property="role" value="USER" />


<h1>Adding a new client</h1>
<form method="post" action="${pageContext.request.contextPath}/addclient" autocomplete="off">
    <div class="form-group">
        <label for="name">Name</label>
        <input name="name" type="text" class="form-control" id="name" value="<jsp:getProperty name="client" property="name" />">
    </div>
    <div class="form-group">
        <label for="phoneNumber">Phone Number</label>
        <input name="phoneNumber" type="text" class="form-control" id="phoneNumber" value="<jsp:getProperty name="client" property="phoneNumber" />">
    </div>
    <div class="form-group">
        <label for="email">E-mail</label>
        <input name="email" type="text" class="form-control" id="email" value="<jsp:getProperty name="client" property="email" />">
    </div>
    <div class="form-group">
        <label for="login">Login</label>
        <input name="login" type="text" class="form-control" id="login" value="<jsp:getProperty name="client" property="login" />">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input name="password" type="text" class="form-control" id="password" value="<jsp:getProperty name="client" property="password" />">
    </div>
    <div class="form-group">
        <label for="role">Role</label>
        <input name="role" type="text" class="form-control" id="role" value="<jsp:getProperty name="client" property="role" />">
    </div>
    <button type="submit" class="btn btn-primary">Add</button>
</form>

