<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean id="art" class="com.inno.pojo.Art" />
    <c:set target="${art}" property="name" value="NewArt" />
    <jsp:setProperty name="art" property="price" value="0" />


<h1>Adding a new art</h1>
<form method="post" action="${pageContext.request.contextPath}/addart" autocomplete="off">
    <div class="form-group">
        <label for="name">Name</label>
        <input name="name" type="text" class="form-control" id="name" value="<jsp:getProperty name="art" property="name" />">
    </div>
    <div class="form-group">
        <label for="price">Price</label>
        <input name="price" type="text" class="form-control" id="price" value="<jsp:getProperty name="art" property="price" />">
    </div>
    <button type="submit" class="btn btn-primary">Add</button>
</form>

