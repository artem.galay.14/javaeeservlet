<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<ul class="list-group">
   <li class="list-group-item">${art.id}</li>
   <li class="list-group-item">${art.name}</li>
   <li class="list-group-item">${art.price}</li>
</ul>
<a href="${pageContext.request.contextPath}/updateart?id=${art.id}">Update</a>

<br>
<a href="/">Main page</a>
