<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<table class="table">
   <thead>
   <tr>
      <th>id</th>
      <th>name</th>
      <th>phone number</th>
      <th>e-mail</th>
   </tr>
   </thead>
   <tbody>
   <c:forEach var="client" items="${clients}">
      <tr>
         <td scope="row">${client.id}</td>
         <td>${client.name}</td>
         <td>${client.phoneNumber}</td>
         <td>${client.email}</td>
         <td><a href="${pageContext.request.contextPath}/showclient?id=${client.id}">Details</a></td>
         <td><form method="POST" action="${pageContext.request.contextPath}/deleteclient?id=${client.id}"><button type="submit">Delete</button></form></td>
      </tr>
   </c:forEach>
   </tbody>
</table>

<br>
<a href="/">Main page</a>