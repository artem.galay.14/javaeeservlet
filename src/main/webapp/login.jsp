<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean id="client" class="com.inno.pojo.Client" />
    <jsp:setProperty name="client" property="login" value="login" />
    <jsp:setProperty name="client" property="password" value="qwerty" />

<h1>Log in</h1>
<form method="get" action="${pageContext.request.contextPath}/checkclient" autocomplete="off">
    <div class="form-group">
        <label for="login">Login</label>
        <input name="login" type="text" class="form-control" id="login" value="<jsp:getProperty name="client" property="login" />">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input name="password" type="text" class="form-control" id="password" value="<jsp:getProperty name="client" property="password" />">
    </div>
    <button type="submit" class="btn btn-primary">Log in</button>
</form>

