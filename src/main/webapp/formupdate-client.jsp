<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean id="updClient" class="com.inno.pojo.Client" />
    <jsp:setProperty name="updClient" property="name" value="${client.name}" />
    <c:set target="${updClient}" property="phoneNumber" value="${client.phoneNumber}" />
    <c:set target="${updClient}" property="email" value="${client.email}" />

<h1>Update existing client</h1>
<form method="post" action="${pageContext.request.contextPath}/updateclient?id=${client.id}" autocomplete="off">
    <div class="form-group">
        <label for="name">Name</label>
        <input name="name" type="text" class="form-control" id="name" value="<jsp:getProperty name="updClient" property="name" />">
    </div>
    <div class="form-group">
        <label for="phoneNumber">Phone Number</label>
        <input name="phoneNumber" type="text" class="form-control" id="phoneNumber" value="<jsp:getProperty name="updClient" property="phoneNumber" />">
    </div>
    <div class="form-group">
        <label for="email">E-mail</label>
        <input name="email" type="text" class="form-control" id="email" value="<jsp:getProperty name="updClient" property="email" />">
    </div>
<button type="submit" class="btn btn-primary">Update</button>
</form>

