<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<table class="table">
   <thead>
   <tr>
      <th>id</th>
      <th>name</th>
      <th>price</th>
   </tr>
   </thead>
   <tbody>
   <c:forEach var="art" items="${arts}">
      <tr>
         <td scope="row">${art.id}</td>
         <td>${art.name}</td>
         <td>${art.price}</td>
         <td><a href="${pageContext.request.contextPath}/showart?id=${art.id}">Details</a></td>
         <td><form method="POST" action="${pageContext.request.contextPath}/deleteart?id=${art.id}"><button type="submit">Delete</button></form></td>
      </tr>
   </c:forEach>
   </tbody>
</table>

<br>
<a href="/">Main page</a>