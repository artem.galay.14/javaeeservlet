<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<ul class="list-group">
   <li class="list-group-item">${client.id}</li>
   <li class="list-group-item">${client.name}</li>
   <li class="list-group-item">${client.phoneNumber}</li>
   <li class="list-group-item">${client.email}</li>
</ul>
<a href="${pageContext.request.contextPath}/updateclient?id=${client.id}">Update</a>

<br>
<a href="/">Main page</a>
