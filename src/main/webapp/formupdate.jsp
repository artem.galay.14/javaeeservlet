<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:useBean id="updArt" class="com.inno.pojo.Art" />
    <c:set target="${updArt}" property="name" value="${art.name}" />
    <jsp:setProperty name="updArt" property="price" value="${art.price}" />


<h1>Update existing art</h1>
<form method="post" action="${pageContext.request.contextPath}/updateart?id=${art.id}" autocomplete="off">
    <div class="form-group">
        <label for="name">Name</label>
        <input name="name" type="text" class="form-control" id="name" value="<jsp:getProperty name="updArt" property="name" />">
    </div>
    <div class="form-group">
        <label for="price">Price</label>
        <input name="price" type="text" class="form-control" id="price" value="<jsp:getProperty name="updArt" property="price" />">
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>

